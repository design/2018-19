# 2018-19

Every year Constant commissions another designer or design collective using Free, Libre and Open Source tools, to work on the two-monthly flyers for Constant_V.

Between 2018 and 2019, we collaborated with [D-E-A-L](http://www.d-e-a-l.eu), a Brussels based collective comprised of Quentin Jumelin and Morgane Le Ferec. In their designs, they experimented with different ways to traverse multiple flyers gathered on a single printed sheet, which resulted in four subtly different variations per edition. Each of the invitations also included a plotter-printed layer, adding texture and materiality to the invitations. In addition, D-E-A-L was responsible for the design of the pink Constant_V catalog.

Since completing the series, D-E-A-L continues to work with various tools including Open Source software.

Designer Arshia Azmat will be responsible for Constant_V flyers and more in 2019-2020.
